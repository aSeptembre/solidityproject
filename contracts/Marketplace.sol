// SPDX-License-Identifier: GPL-3.0

pragma solidity >=0.8.2 <0.9.0;

contract Marketplace {
    enum ShippingStatus {Pending, Shipped, Delivered}
    ShippingStatus public status;

    event MissionComplete();
    event StatusEvent (ShippingStatus);
    
    address public owner;

    constructor() {
        owner = msg.sender;
        status = ShippingStatus.Pending;
    }

    function Shipped() public {
        require(msg.sender == owner, "Only owner can call this function");
        status = ShippingStatus.Shipped;
    }

    function Delivered() public {
        require(msg.sender == owner, "Only owner can call this function");
        status = ShippingStatus.Delivered;
    }

    function getStatus() public view returns (ShippingStatus) {
        require(msg.sender == owner, "Only owner can call this function");
        return status;
    }

    function Status() public payable {
        require(msg.sender != owner, "Only customers can call this function");
        emit StatusEvent(status);
    }
}